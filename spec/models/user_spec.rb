require 'spec_helper'

describe User do



  let(:no_name) { User.new(password_digest: "digest", session_token: "token") }
  let(:no_password_digest) { User.new(username: "name", session_token: "token") }
  let(:no_session_token) { User.new(password_digest: "digest", username: "name") }

  let(:good_user) { User.new(username: "name",
                           password: "123123")}


   describe "associations" do
     it { should have_many(:goals) }
   end

  describe "validations" do

    it "should have a username" do
      expect(no_name).not_to be_valid

      no_name.save
      expect(no_name.errors.full_messages).to include("Username can't be blank")
    end


    it "should require unique username" do
      create(:user, username: "double_name")
      not_unique = User.new(username: "double_name", password: "123123")

      expect(not_unique).not_to be_valid
    end

    it "should have a password_digest" do
      expect(no_password_digest).not_to be_valid

      no_password_digest.save
      expect(no_password_digest.errors.full_messages).to include(
        "Password digest can't be blank")
    end

    it "should have session_token" do
      good_user.valid?
      expect(good_user.session_token).not_to be_nil
    end

    it "should have password length at least 6" do
      short_password = User.new(username: "name", password_digest: "digest", session_token: "token", password: "h")
      expect(short_password).not_to be_valid

      short_password.save
      expect(short_password.errors.full_messages).to include(
        "Password is too short (minimum is 6 characters)")
    end

    it "should save if user is valid" do

      expect(good_user).to be_valid
    end

    describe "#is_password?" do
      it "should reject a false value" do
        expect(good_user.is_password?('not_it')).to be_false
      end

      it "should accept a correct value" do
        expect(good_user.is_password?("123123")).to be_true
      end
    end

  end

  describe "attributes" do

    describe "#encrypt" do
      let(:bcrypt) { User::BCrypt::Password }


      it "should call BCrypt" do
        secret = good_user.password

        expect(bcrypt).to receive(:create).with(secret)
        good_user.encrypt(secret)
      end

      it "should encrypt password" do
        test_word = "test_word"

        bcrypt.stub(:create) { |secret| "not_test_word" }

        expect(good_user.encrypt(test_word)).not_to eq(test_word)
      end

    end


  end
end
