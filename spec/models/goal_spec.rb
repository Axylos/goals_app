require 'spec_helper'

describe Goal do

  describe "its attributes" do
    it { should respond_to(:author) }
    it { should respond_to(:accomplished?) }
    it { should respond_to(:public?) }
    it { should respond_to(:content) }
  end

end
