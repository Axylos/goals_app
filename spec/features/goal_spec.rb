require 'spec_helper'

feature "Goals CRUD" do

  before(:each) do
    user1 = create(:user)
    sign_in_user(user1)
  end

  feature "adds a goal" do

    scenario "has a goal form" do
      expect(page).to have_content "Add a Goal"
    end

    scenario " shows a goal that has been added" do
      fill_in "Title", with: "goal_title"
      fill_in "Description", with: "detailed goal"

      click_on "Create Goal"
      expect(page).to have_content "goal_title"
    end
  end


  feature "view a goal" do

    scenario "has a link to the goal body" do
      click_on "goal_title"

      expect(page).to have_content "goal_title"
      expect(page).to have_content "detailed goal"
    end

   scenario "has a link to go back to user's goals" do
      click_on "Back to my Goals"

      expect(page).to have_content "Your Goals"
    end
  end
end