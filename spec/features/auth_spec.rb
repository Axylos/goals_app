require 'spec_helper'

feature "the signup process" do

  scenario "has a new user page" do
    visit new_user_url
    expect(page).to have_content "Sign Up"
  end

  feature "signing up a user" do
    before(:each) do
      visit new_user_url
      fill_in 'username', with: 'testing_username'
      fill_in 'password', with: '123123'

      click_button("Sign Up")
    end

    scenario "redirects to the user show page, after signup" do

      expect(page).to have_content "Your Goals"
    end

    scenario "shows username on signup" do
      expect(page).to have_content "testing_username"
    end
  end
end

feature "the sign in process" do
  let(:user) { FactoryGirl.create(:user) }

  scenario "has a sign in page" do
    visit new_session_url
    expect(page).to have_content "Sign In"
  end

  feature "signs in a user with invalid credentials" do
    before(:each) do
      visit new_session_url
    end

    scenario "missing username" do
      fill_in 'password', with: 'secret'
      click_on 'Sign In'

      expect(page).to have_content "Invalid Credentials"
    end

    scenario "missing password" do
      fill_in 'username', with: 'teseter'
      click_on 'Sign In'

      expect(page).to have_content "Invalid Credentials"
    end

    scenario "good username bad password" do
      fill_in 'username', with: "test_user"
      fill_in "password", with: "234234"
      click_on 'Sign In'

      expect(page).to have_content "Invalid Credentials"
    end

    scenario "bad username" do
      fill_in 'username', with: "user_test"
      fill_in "password", with: "123123"
      click_on 'Sign In'

      expect(page).to have_content "Invalid Credentials"
    end
  end

  feature "signs in a user with valid credentials" do

    scenario "signs in with valid credentials" do
      create(:user)
      visit new_session_url
      fill_in 'username', with: "test_user"
      fill_in "password", with: "123123"
      click_button "Sign In"

      expect(page).to have_content "Your Goals"
      expect(page).to have_content "test_user"
    end

    feature "logging out" do

      it "begins with logged out state" do
        create(:user)

        visit "/goals"
        expect(page).to have_content("Sign In")
      end

      it "signs user out" do
        create(:user)
        visit new_session_url
        fill_in 'username', with: "test_user"
        fill_in "password", with: "123123"
        click_on "Sign In"

        click_on "Logout"
        expect(page).to have_content("Sign In")

      end

    end
  end



end
