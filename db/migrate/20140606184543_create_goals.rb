class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.integer :user_id
      t.string :content
      t.boolean :public_status
      t.boolean :accomplished_status

      t.timestamps
    end
  end
end
