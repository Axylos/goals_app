class ChangeGoalsContent < ActiveRecord::Migration
  def change
    rename_column :goals, :content, :body
  end
end
