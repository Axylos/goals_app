TodoApp::Application.routes.draw do

  shallow do
    resources :users, only: [:new, :create, :show] do
      resources :goals
    end
  end
  resource :session, only: [:new, :create, :destroy]

  resources :goals, only: [:index]
end
