# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
TodoApp::Application.config.secret_key_base = 'b39ed406593f3a59f8412df3102f56d416954ef5d5cb3fbae0d8f7d69e775e0d830d7c1a9cbba85ab81165d9739d4e3f4ed65c4129c96742632508fcf2ad2f58'
