class GoalsController < ApplicationController

  def index

  end

  def create
    @goal = Goal.new(goal_params)
    @goal.author = current_user
    if @goal.save
      redirect_to user_url(@goal.author)
    else
      flash[:errors] = @goal.errors.full_messages
      redirect_to user_url(@goal.author)
    end
  end


  private

  def goal_params
    params.require(:goal).permit(:title, :body)
  end
end
