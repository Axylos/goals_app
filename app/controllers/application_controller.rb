class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :ensure_signed_in

  helper_method :signed_in?, :current_user

  def current_user
    User.find_by(session_token: session[:token])
  end

  def signed_in?
    !!current_user
  end

  def sign_in_user(user)
    session[:token] = user.reset_session_token!
  end


  private

  def ensure_signed_in
    redirect_to new_session_url unless signed_in?
  end
end
