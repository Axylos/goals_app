class UsersController < ApplicationController

  skip_filter :ensure_signed_in, only: [:new, :create]

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      sign_in_user(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    @goal = @user.goals.new
    render :show
  end

  private

  def user_params
    params.require(:user).permit(:password, :username)
  end
end
