class Goal < ActiveRecord::Base

  belongs_to :author, class_name: "User", foreign_key: :user_id

  def accomplished?
    !!@accomplished_status
  end

  def public?
    !!@public_status
  end

end
