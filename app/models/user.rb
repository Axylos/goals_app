class User < ActiveRecord::Base

  attr_reader :password

  has_many :goals, class_name: "Goal", foreign_key: :user_id

  validates :username, :password_digest, :session_token, presence: true
  validates :username, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true }
  before_validation :ensure_session_token

  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save
    self.session_token
  end

  def password=(word)
    @password = word
    self.password_digest = encrypt(word)
  end

  def encrypt(word)
    BCrypt::Password.create(word)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end


  private

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end

end